- Compare ironpath's absolutifying algorithm with the one I made for path_abs
- Remember Windows always resolves `..` lexically, like Plan9.
- Implement Borrow<T> as well as AsRef<T> for completeness.
- Should we handle Windows prefixes that don't exist, like UNC paths to
  unresolvable servers or unmapped drive letters?

Useful links:

- https://github.com/vitiral/path_abs/issues/21#issuecomment-410967402
- https://blogs.msdn.microsoft.com/oldnewthing/20100506-00/?p=14133/
- https://googleprojectzero.blogspot.com/2016/02/the-definitive-guide-on-win32-to-nt.html
